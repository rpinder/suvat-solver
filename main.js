function clear_inputs() {
    for (var i = 0; i < 5; i++) {
        document.getElementById(String(i) + '-input').value = '';
    }
}

function non_null(array) {
    for (i=0; i<array.length; i++) {
        if (variables[array[i]] === null) {
            return  false;
        }
    }
    return true;
}


function read_variables() {
    values = [];
    variables = [];

    for (var i = 0; i < 5; i++) {
        values[i] = document.getElementById(String(i) + '-input').value;
        variables[i] = values[i] != "" ? Number(document.getElementById(String(i) + '-input').value) : null;
    }
}

function update_variables() {
    variables = [null,null,null,null,null];
    read_variables();

    if (non_null([0, 1, 2])) {
        variables[4] = (2 * variables[0])/(variables[1] + variables[2]);
        variables[3] = (Math.pow(variables[2],2) - Math.pow(variables[1],2))/(2 * variables[0]);
    }
    if (non_null([1, 2, 3])) {
        variables[4] = (variables[2] - variables[1]) / variables[3];
        variables[0] = (Math.pow(variables[1],2) + Math.pow(variables[2],2))/(2 * variables[3]);
    }
    if (non_null([2, 3, 4])) {
        variables[1] = variables[2] - (variables[3] * variables[4]);
        variables[0] = variables[2] * variables[4] - 0.5 * variables[3] * Math.pow(variables[4],2);
    }
    if (non_null([0, 1, 3])) {
        variables[4] = (Math.pow(2 * variables[3] * variables[0] + Math.pow(variables[1], 2), 0.5) - variables[1])/(variables[3]);
        variables[2] = Math.pow(Math.pow(variables[1], 2) + 2 * variables[3] * variables[0], 0.5);
    }
    if (non_null([0, 1, 4])) {
        variables[3] = (2 * (variables[0] - variables[1] * variables[4])) / Math.pow(variables[4],2);
        variables[2] = (2 * variables[0])/(variables[4]) - variables[1];
    }
    if (non_null([1, 2, 4])) {
        variables[3] = (variables[2] - variables[1])/(variables[4]);
        variables[0] = (variables[4] / 2) * (variables[1] + variables[2]);
    }
    if (non_null([1, 3, 4])) {
        variables[2] = variables[1] + variables[3] * variables[4];
        variables[0] = variables[1] * variables[4] + 0.5 * variables[3] * Math.pow(variables[4], 2);
    }

    for (var i = 0; i < 5; i++) {
        document.getElementById(String(i)+ '-input').value = String(variables[i]);
    }
}

